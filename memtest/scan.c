/*------------------------------------------------------------------------------
            CRIBFLEX - Cosmic Ray-Induced Bit FLipping EXperiment
                       Beaglebone Black Memory Scanner
                           Andrew Pellegrino, 2016

Program allocates an array of size MEMSIZE of unsigned 8-bit ints initialized to
the value DEFVAL and scans approximately once per hour to check whether a value
has changed while recording the results to a .csv file. If a change is found,
the new value is also recorded.

Currently this program only records the new value for the latest location in
memory at which an event occurred since the last scan. Since even one event in
the span of an hour is extremely unlikely, this is fine for our purposes.

Must be compiled with the C99 standard ( --std=c99 for gcc ) due to the printf()
calls.

On Linux, make sure to remove the process for this program from the OOM killer
so that it may run undisturbed. The bash script "doscan.sh" can do this for the
user.
------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/*
Size of the allocated array in bytes. BBone memory is 512 MiB = 536870912 bytes
*/
#define MEMSIZE 200000000
// Default memory value, allows for flips from 0 to 1, or 1 to 0
#define DEFVAL 0xF0F0F0F0
// Seconds to wait in between scans
#define SLEEPTIME 3596

int main() {
	/* 
	volatile keyword tells the compiler that the variable may change even
	though the program does not change it. This is crucial to the program
	*/
	volatile unsigned int *arr;
	
	int arrsize = MEMSIZE/sizeof(unsigned int);
	int errors = 0;
	int value = -1;
	int i;

	time_t seconds;
	
	FILE *fp;
	char path[50];
	sprintf(path, "/usr/local/memtest/log_%1d.csv", time(NULL));
	fp = fopen(path, "w+");
	fprintf(fp, "UNIX time,cumulative errors,message\n");
	fclose(fp);
	printf("Initializing...\n");
	arr = (volatile unsigned int *) malloc(arrsize,sizeof(unsigned int));

	for(i=0; i<arrsize; i++ ) {
		arr[i] = DEFVAL;
	}
	
	printf("Running...\n");

	while(1) {
		seconds = time(NULL);

		for(i=0; i<arrsize; i++) {
			if (arr[i] != DEFVAL) {
				value = arr[i];
				arr[i] = DEFVAL;
				errors++;
			}
		}
		fp = fopen(path, "a");
		if (value != -1) fprintf(fp,"%1d,%d,%x\n",seconds,errors,value);
		else fprintf(fp,"%1d,%d,\n",seconds,errors);
		fclose(fp);
		
		value = -1;
		printf("Finished a scan\n");
		sleep(SLEEPTIME);
	}

	free((void*)arr);
	return 0;
}
