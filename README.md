# CRIBFLEX #

CRIBFLEX (Cosmic Ray-Induced Bit-FLipping EXperiment) is an experiment to measure [soft errors](https://en.wikipedia.org/wiki/Soft_error) in computer memory. Our current platform is a [BeagleBone Black](https://beagleboard.org/black) running Debian Linux.